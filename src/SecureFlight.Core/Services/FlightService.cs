﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services
{
    public class FlightService : BaseService<Flight>, IFlightService
    {
        public FlightService(IRepository<Flight> repository) : base(repository)
        {

        }

        public async Task<Flight> GetFlightByOriginAndDestination(string origin, string destination)
        {
            var flightsResult = await GetAllAsync();
            var flights = flightsResult.Result;

            var flight = flights.FirstOrDefault(x => x.DestinationAirport == destination && x.OriginAirport == origin);
            return flight;
        }
    }
}
