﻿using SecureFlight.Core.Entities;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IFlightService : IService<Flight>
    {
        Task<Flight> GetFlightByOriginAndDestination(string origin, string destination);
    }
}