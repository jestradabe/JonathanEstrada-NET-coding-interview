﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FlightsController : ControllerBase
    {
        private readonly IFlightService _flightService;

        public FlightsController(IFlightService flightService)
        {
            _flightService = flightService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> Get()
        {
            var flights = (await _flightService.GetAllAsync()).Result
                .Select(x => new FlightDataTransferObject
                {
                    Id = x.Id,
                    ArrivalDateTime = x.ArrivalDateTime,
                    Code = x.Code,
                    FlightStatusId = (int) x.FlightStatusId,
                    DepartureDateTime = x.DepartureDateTime,
                    DestinationAirport = x.DestinationAirport,
                    OriginAirport = x.OriginAirport
                });

            return Ok(flights);
        }

        [HttpGet("{origin}/{destination}")]
        [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(string origin, string destination)
        {
            var flight = await _flightService.GetFlightByOriginAndDestination(origin, destination);

            if (flight is null)
                return NotFound();

            var flightDto = new FlightDataTransferObject
            {
                Id = flight.Id,
                ArrivalDateTime = flight.ArrivalDateTime,
                Code = flight.Code,
                FlightStatusId = (int)flight.FlightStatusId,
                DepartureDateTime = flight.DepartureDateTime,
                DestinationAirport = flight.DestinationAirport,
                OriginAirport = flight.OriginAirport
            };

            return Ok(flightDto);
        }
    }
}
