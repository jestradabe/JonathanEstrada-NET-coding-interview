using Microsoft.EntityFrameworkCore;
using SecureFlight.Core.Entities;
using SecureFlight.Infrastructure;
using SecureFlight.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using Xunit;

namespace SecureFlight.Test
{
    public class AirportTests
    {
        [Fact]
        public void Update_Succeeds()
        {
            var options = new DbContextOptionsBuilder<SecureFlightDbContext>()
            .UseInMemoryDatabase(databaseName: "MovieListDatabase")
            .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = new SecureFlightDbContext(options))
            {
                context.Airports.AddRange(new List<Airport>
                {
                    new Airport
                    {
                        City = "Anapa",
                        Country = "Russia",
                        Code = "AAQ",
                        Name = "Anapa Vityazevo"
                    },
                    new Airport
                    {
                        City = "New York",
                        Country = "USA",
                        Code = "JFK",
                        Name = "John F Kennedy International"
                    },
                    new Airport
                    {
                        City = "Abadan",
                        Country = "Iran",
                        Code = "ABD",
                        Name = "Abadan"
                    },
                    new Airport
                    {
                        City = "Albuquerque",
                        Country = "USA",
                        Code = "ABQ",
                        Name = "Albuquerque International Sunport"
                    }
                });

                context.SaveChanges();
            }

            //var repository = new BaseRepository<Airport>(context);
        }
    }
}
